var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/test');
var Sniff = mongoose.model('Sniff', {timestamp: String, mac: String, rssi0: String, rssi1: String, sniffer: String});


io.sockets.on('connection', function(socket){
	console.log('raspberry pi connected' + socket);

	socket.on('sendData', function(data) {
		var data = JSON.parse(data);

		var dataToStore = {
			timestamp : data.timestamp,
			mac : data.mac,
			rssi0 : data.rssi0,
			rssi1 : data.rssi1,
			sniffer : data.sniffer
		};

		var sniffData = new Sniff(dataToStore);
		sniffData.save(function (err) {
			if (err) {
				console.log('mislukt');
			} else {
				// console.log('gelukt');
			}
		});
	});

	socket.on('pingEvent', function(data) {
		try {
			var data = JSON.parse(data);
			var lineToWrite = "Host: " + data.sniffer + " @ " + data.ip + "\n";

			var fs = require('fs');
			fs.appendFile('/home/go84/ips.txt', lineToWrite, function (err) {

			});

		} catch (err) {

		}

	});


});

http.listen(3000, function(){
    console.log('listen on:3000');
});





